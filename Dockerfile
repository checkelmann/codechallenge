FROM ubuntu:18.04

RUN apt-get update && apt-get install cmake wget curl --yes
RUN wget https://dl.bintray.com/conan/installers/conan-ubuntu-64_1_32_1.deb
RUN apt-get install ./conan-ubuntu-64_1_32_1.deb --yes